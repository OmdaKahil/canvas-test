import React from "react";
import "./App.css";
import Story from "./container/story";
import Store from "./redux/store/store";
import { Provider } from "react-redux";

const App = () => {
  return (
    <Provider store={Store}>
      <Story />
    </Provider>
  );
};

export default App;
