import React from "react";
import { Toast } from "react-bootstrap";

interface Props {
  show?: boolean;
  style?: any;
  title?: string;
  body?: string;
}
const Alert = ({ show, style, title, body }: Props) => {
  return (
    <>
      <Toast style={style} show={show}>
        <Toast.Header>
          <img src="holder.js/20x20?text=%20" className="rounded mr-2" alt="" />
          <strong className="mr-auto">{title}</strong>
        </Toast.Header>
        <Toast.Body>{body}</Toast.Body>
      </Toast>
    </>
  );
};

export default Alert;
