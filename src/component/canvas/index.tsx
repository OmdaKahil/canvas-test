import { useEffect, useRef, useState } from "react";

type Coordinates = {
  x: number;
  y: number;
};

interface Props {
  color: string;
  url?: string;
  download: number;
  error: any;
}
const CanvasComponent = (props: Props) => {
  const { color, url, download, error } = props;
  const canvasRef = useRef<HTMLCanvasElement>(null);
  const [context, setContext] = useState<CanvasRenderingContext2D | null>(null);

  useEffect(() => {
    let mouseDown: boolean = false;
    let start: Coordinates = { x: 0, y: 0 };
    let end: Coordinates = { x: 0, y: 0 };
    let canvasOffsetLeft: number = 0;
    let canvasOffsetTop: number = 0;

    function handleMouseDown(evt: MouseEvent) {
      console.log(url);
      console.log(color);
      if (url && color) {
        mouseDown = true;

        start = {
          x: evt.clientX - canvasOffsetLeft,
          y: evt.clientY - canvasOffsetTop,
        };

        end = {
          x: evt.clientX - canvasOffsetLeft,
          y: evt.clientY - canvasOffsetTop,
        };
      } else {
        if (!url) error("Oops", "please pick a picture");
        else error("Oops", "please pick a color");
      }
    }

    function handleMouseUp(evt: MouseEvent) {
      mouseDown = false;
      if (url) error("Hip Horrey", "you can download you image :) ");
    }

    function handleMouseMove(evt: MouseEvent) {
      if (mouseDown && context) {
        start = {
          x: end.x,
          y: end.y,
        };

        end = {
          x: evt.clientX - canvasOffsetLeft,
          y: evt.clientY - canvasOffsetTop,
        };

        // Draw our path
        context.beginPath();
        context.moveTo(start.x, start.y);
        context.lineTo(end.x, end.y);
        context.strokeStyle = color;
        context.lineWidth = 3;
        context.stroke();
        context.closePath();
      }
    }

    if (canvasRef.current) {
      const renderCtx = canvasRef.current.getContext("2d");

      if (renderCtx) {
        canvasRef.current.addEventListener("mousedown", handleMouseDown);
        canvasRef.current.addEventListener("mouseup", handleMouseUp);
        canvasRef.current.addEventListener("mousemove", handleMouseMove);

        canvasOffsetLeft = canvasRef.current.offsetLeft;
        canvasOffsetTop = canvasRef.current.offsetTop;

        setContext(renderCtx);
      }
    }

    return function cleanup() {
      if (canvasRef.current) {
        canvasRef.current.removeEventListener("mousedown", handleMouseDown);
        canvasRef.current.removeEventListener("mouseup", handleMouseUp);
        canvasRef.current.removeEventListener("mousemove", handleMouseMove);
      }
    };
  }, [context, color, url]);

  useEffect(() => {
    if (url) drawImage(url);
  }, [url]);

  useEffect(() => {
    if (download > 0) downloadImage();
  }, [download]);

  function drawImageProp(
    ctx: any,
    img: any,
    x: number,
    y: number,
    w: number,
    h: number,
    offsetX: any,
    offsetY: any
  ) {
    if (arguments.length === 2) {
      x = y = 0;
      w = ctx.canvas.width;
      h = ctx.canvas.height;
    }

    // default offset is center
    offsetX = typeof offsetX === "number" ? offsetX : 0.5;
    offsetY = typeof offsetY === "number" ? offsetY : 0.5;

    // keep bounds [0.0, 1.0]
    if (offsetX < 0) offsetX = 0;
    if (offsetY < 0) offsetY = 0;
    if (offsetX > 1) offsetX = 1;
    if (offsetY > 1) offsetY = 1;

    var iw = img.width,
      ih = img.height,
      r = Math.min(w / iw, h / ih),
      nw = iw * r, // new prop. width
      nh = ih * r, // new prop. height
      cx,
      cy,
      cw,
      ch;

    // calc source rectangle
    cw = iw / (nw / w);
    ch = ih / (nh / h);

    cx = (iw - cw) * offsetX;
    cy = (ih - ch) * offsetY;

    // make sure source rectangle is valid
    if (cx < 0) cx = 0;
    if (cy < 0) cy = 0;
    if (cw > iw) cw = iw;
    if (ch > ih) ch = ih;

    // fill image in dest. rectangle
    ctx.drawImage(img, cx, cy, cw, ch, x, y, w, h);
  }

  const downloadImage = () => {
    let image = canvasRef?.current
      ?.toDataURL("image/png")
      ?.replace("image/png", "image/octet-stream");

    if (image) window.location.href = image;
  };
  const drawImage = (url: string) => {
    const base_image = new Image();
    base_image.src = url;
    base_image.onload = () =>
      drawImageProp(
        context,
        base_image,
        0,
        0,
        window.innerWidth / 4,
        window.innerHeight * 0.87,
        0,
        0
      );
  };
  return (
    <>
      <div
        style={{
          textAlign: "center",
        }}
      >
        <canvas
          id="canvas"
          ref={canvasRef}
          width={window.innerWidth / 4}
          height={window.innerHeight * 0.87}
          style={{
            marginTop: 10,
            background: "#05445E",
            borderRadius: 5,
          }}
        ></canvas>
      </div>
    </>
  );
};

export default CanvasComponent;
