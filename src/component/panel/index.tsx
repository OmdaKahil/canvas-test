import styles from "./index.module.css";
import "bootstrap/dist/css/bootstrap.css";
import { Container, Row, Col, Button } from "react-bootstrap";
import UploadButton from "../uploadButton/index";
import { useDispatch, useSelector } from "react-redux";
import { counterActions } from "../../redux/slices";

interface Props {
  onImageSelected: any;
  onColorPicked: any;
  color: string;
  handleDownloadButton: Function;
}

const Panel = (props: Props) => {
  const { onImageSelected, color, onColorPicked, handleDownloadButton } = props;
  const dispatch = useDispatch();
  const counterNumber = useSelector((state: any) => state.counter.counter);
  return (
    <>
      <div className={styles.container}>
        <Container>
          <Row className={styles.pannel}>
            <Col>
              <UploadButton
                title={"upload"}
                onImageSelected={(e: any) => {
                  onImageSelected(URL.createObjectURL(e.target.files[0]));
                }}
              ></UploadButton>
              <Button
                style={{
                  padding: 7,
                  paddingRight: 10,
                  paddingLeft: 10,
                  borderRadius: 4,
                  color: "#fbe0c4",
                  backgroundColor: "#2978b5",
                  alignItems: "center",
                  justifyContent: "center",
                  marginLeft: 20,
                }}
                onClick={() => {
                  handleDownloadButton();
                  dispatch(
                    counterActions.setCounter({ counter: counterNumber + 1 })
                  );
                }}
              >
                {"Download"}
              </Button>
            </Col>
            <Col>
              <input
                style={{ marginLeft: 50 }}
                type="color"
                value={color}
                onChange={onColorPicked}
              />
              <input className={styles.Panel} type="text" value={color} />
            </Col>
            <Col>
              <span
                style={{
                  marginLeft: 20,

                  color: "#fbe0c4",
                }}
              >
                you tried to download {`${counterNumber}`} images
              </span>
            </Col>
          </Row>
        </Container>
      </div>
    </>
  );
};

export default Panel;
