import React from "react";
import FormFileInput from "react-bootstrap/esm/FormFileInput";
import styles from "./index.module.css";
interface uploadButtonProps {
  onImageSelected: any;
  title?: string;
}
const UploadButton = ({ onImageSelected, title }: uploadButtonProps) => {
  return (
    <>
      <label style={{ color: "#fbe0c4" }}>
        {"upload"}
        <input
          className={styles.custom_file_upload}
          type="file"
          accept={"image/*"}
          onChange={onImageSelected}
        />
      </label>
    </>
  );
};

export default UploadButton;
