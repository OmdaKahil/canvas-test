import React, { useRef, useState } from "react";
import CanvasComponent from "../component/canvas";
import { Button } from "react-bootstrap";
import Panel from "../component/panel";
import Alert from "../component/alert";

const Story = () => {
  const [color, setColor] = useState("#000");
  const [url, setUrl] = useState<string>();
  const [download, setDownload] = useState(0);
  const [alert, setAlert] = useState({
    show: false,
    title: "",
    body: "",
  });

  const errorHandle = (title: string, body: string) => {
    setAlert({
      show: true,
      title,
      body,
    });

    setTimeout(() => {
      setAlert({
        show: false,
        title,
        body,
      });
    }, 2000);
  };
  return (
    <>
      <Panel
        handleDownloadButton={() => {
          !url
            ? errorHandle(
                "Welcome to my Testing Site",
                "Kindly upload an image.. Edit It then download :)"
              )
            : setDownload(download + 1);
        }}
        onImageSelected={(url: string) => {
          setUrl(url);
        }}
        color={color}
        onColorPicked={(e: any) => {
          setColor(e.target.value);
        }}
      />
      <CanvasComponent
        download={download}
        color={color}
        url={url}
        error={errorHandle}
      />

      <Alert
        style={{ position: "absolute", top: "20%", right: "2%" }}
        show={alert.show}
        title={alert.title}
        body={alert.body}
      ></Alert>
    </>
  );
};

export default Story;
