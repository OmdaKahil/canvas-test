import { createSlice } from "@reduxjs/toolkit";
import { CounterState } from "./counter.type";


const initialState: CounterState = {
    counter: 0
};

const counterSlice = createSlice({
    name: 'counterSlice',
    initialState: initialState,
    reducers: {
        setCounter: (state, action) => {
            return { ...state, ...action.payload };
        },
        reset: () => initialState,
    },
});

export const counterReducer = counterSlice.reducer;
export const counterActions = { ...counterSlice.actions };
