import { counterReducer, counterActions } from './counter.slice';
import { CounterState } from './counter.type';

export type Counter = CounterState;
export { counterActions, counterReducer };
